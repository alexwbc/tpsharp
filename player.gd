extends KinematicBody

var c_logic

func _enter_tree():
	c_logic = $logic_c
	get_node("/root/main/output/").text = "using GDScript, press Enter to switch"
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
	if Input.is_action_just_pressed("jump"):
		get_node("logic_c").phy_plug = "Jump"
	
	if Input.is_action_just_pressed("debug_a"):
		if c_logic.sw:
			get_node("/root/main/output/").text = "using GDScript, press Enter to switch"
			c_logic.sw = false
		else:
			get_node("/root/main/output/").text = "using C#, press Enter to switch"
			c_logic.sw = true


func _input(event):
	if c_logic.sw:
		return


	var dir = Vector3()
	var cam_xform = $yaw/cam.get_global_transform().basis
	dir += (Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward"))*cam_xform[2]
	dir += (Input.get_action_strength("move_right") - Input.get_action_strength("move_left"))*cam_xform[0]
	c_logic.dir = dir
	