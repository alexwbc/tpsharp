using Godot;
using System;

public class logic_c : Node
{
// nodes
	private KinematicBody body;//player root
	private Spatial yaw;//camera "arm" turns horizontally
	private Spatial pitch;//camera "arm" turns vertically
	private InterpolatedCamera cam;//the actual camera
	private Position3D cam_pos;//position camera will interpolate
	private Position3D cam_focus;//point where camera fix its gaze
//physics
	private const int g = -40;//gravity
	private const int MAX_SPEED = 12;//the speed, max
	private const int JUMP_SPEED = 20;//the jump... jumpy
	private const int ACCEL= 9;//accellerative acceleration
	private const int DEACCEL= 9;//decellerative decelleration
	private const int AIR_ACCEL = 1;//windy windcelleration
	private const int MAX_SLOPE_ANGLE = 30;//beyond this angle value, we consider it a wall/obstacle
	public Vector3 UP = new Vector3(0,1,0);
	
//	input variables
	private float MOUSE_SPEED = 0.001F;
	
//	camera
	public Vector3 pitch_rot = new Vector3(0.0f,0.0f,0.0f);
	public Vector3 yaw_rot = new Vector3(0.0f,0.0f,0.0f);


	public Vector3 dir = new Vector3(0,0,0);//direction player wants to move
	public Vector3 vel = new Vector3(0,0,0);//actual velocity

	public string phy_plug = null;



	//debug stuff
	public bool sw = false;



public void InitiateNodes(){
	body = GetParent() as KinematicBody;
	yaw = body.GetNode("yaw") as Spatial;
	pitch = yaw.GetNode("pitch") as Spatial;
	cam = yaw.GetNode("cam") as InterpolatedCamera;
	cam_pos = pitch.GetNode("camera_holder") as Position3D;
	cam_focus = body.GetNode("cam_focus") as Position3D;
}
public override void _EnterTree(){
	InitiateNodes();
	base._EnterTree();
}



public override void _Ready()
    {
    }

public override void _Input(InputEvent @event){
	if (@event is InputEventMouseMotion motion){
		var mouse = motion.Relative*MOUSE_SPEED;
		pitch_rot.x = Mathf.Clamp(pitch_rot.x-mouse.y,-0.7f,0.5f);
		pitch.SetRotation(pitch_rot);
		yaw_rot.y -= mouse.x;
		yaw.SetRotation(yaw_rot);

		}
	
	if (!sw){return;}
	dir = new Vector3();
	var cam_xform = cam.GetGlobalTransform().basis;
	dir += (Input.GetActionStrength("move_backward") - Input.GetActionStrength("move_forward"))*cam_xform[2];
	dir += (Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left"))*cam_xform[0];

	}

public void Jump()//if phy_plug calls "Jump", this function will called (once)
	{
		vel.y = JUMP_SPEED;
	}


public override void _Process(float delta){
	cam_pos.LookAt(cam_focus.GetGlobalTransform().origin, UP);
	}


public override void _PhysicsProcess(float delta)
	{
	dir.y = 0;
	dir = dir.Normalized();
	vel.y += delta*g;
	var hvel = vel;
	hvel.y = 0;
	var target = dir * MAX_SPEED;
	var accel = ACCEL;
	if (dir.Dot(hvel) < 0){
		accel = DEACCEL;
		}
	if (!(body.IsOnFloor())){
		accel = AIR_ACCEL;
		}
	hvel = hvel.LinearInterpolate(target, accel * delta);
	vel.x = hvel.x;
	vel.z = hvel.z;
	if (!(phy_plug == null)){
		Call(phy_plug);
		phy_plug = null;
		}
	vel = body.MoveAndSlide(vel, UP,true);
	}








}